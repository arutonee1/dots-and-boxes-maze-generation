import collections
import random
from typing import Deque
from grid import *

directional_inverses = {
    'up': 'down',
    'down': 'up',
    'left': 'right',
    'right': 'left',
}

class TwoStage:
    @staticmethod
    def run(grid: Grid, step: bool = False) -> None:
        if step:
            print(str(grid))
            input()
        while not TwoStage.draw_line(grid, step): pass
        TwoStage.make_perfect(grid, step)

    @staticmethod
    def propagate_happiness(tile: Tile) -> None:
        if tile.happy: return
        happy = 0
        if not tile.up or tile.up.happy: happy += 1
        if not tile.down or tile.down.happy: happy += 1
        if not tile.left or tile.left.happy: happy += 1
        if not tile.right or tile.right.happy: happy += 1
        if happy == 4:
            tile.happy = True # No propagation needed
            return

        walls = 0
        if not tile.up: walls += 1
        if not tile.down: walls += 1
        if not tile.left: walls += 1
        if not tile.right: walls += 1
        if walls >= 2:
            tile.happy = True
            if tile.up: TwoStage.propagate_happiness(tile.up)
            if tile.down: TwoStage.propagate_happiness(tile.down)
            if tile.left: TwoStage.propagate_happiness(tile.left)
            if tile.right: TwoStage.propagate_happiness(tile.right)

    @staticmethod
    def select_unhappy(grid: Grid) -> Tile | None:
        unhappy = [t for row in grid.grid for t in row if not t.happy]
        if unhappy == []: return None
        return random.choice(unhappy)

    @staticmethod
    def can_touch(init_a: Tile, init_b: Tile) -> bool:
        aqueue = collections.deque([init_a])
        bqueue = collections.deque([init_b])
        aseen = set()
        bseen = set()
        while len(aqueue) != 0 and len(bqueue) != 0:
            a = aqueue.popleft()
            b = bqueue.popleft()
            aseen.add(a)
            bseen.add(b)
            if a in bseen or b in aseen: return True
            for attr in ['up', 'down', 'left', 'right']:
                if getattr(a, attr) and getattr(a, attr) not in aseen:
                    aqueue.append(getattr(a, attr))
                if getattr(b, attr) and getattr(b, attr) not in bseen:
                    bqueue.append(getattr(b, attr))
        return False

    @staticmethod
    def draw_line(grid: Grid, step: bool) -> bool:
        r = TwoStage.select_unhappy(grid)
        if not r: return True
        dirs = [a for a in ['up', 'down', 'left', 'right'] if getattr(r, a)
                and not getattr(r, a).happy
                and getattr(r, a) != grid.start
                and getattr(r, a) != grid.end]
        if dirs == []:
            r.happy = True
            return False
        dir = random.choice(dirs)
        other = getattr(r, dir)
        for a in ['up', 'down', 'left', 'right']:
            if getattr(other, a) == r:
                setattr(other, a, None)
        setattr(r, dir, None)
        # NOTE: This is *technically* incorrect, but it provides a result that's close enough
        TwoStage.propagate_happiness(other)
        TwoStage.propagate_happiness(r)
        if not TwoStage.can_touch(r, other):
            setattr(r, dir, other)
            setattr(other, directional_inverses[dir], r)
        if step:
            print(str(grid))
            input()
        return False

    @staticmethod
    def make_perfect(grid: Grid, step: bool) -> None:
        for tile in (t for row in grid.grid for t in row): tile.happy = False
        grid.start.happy = False
        queue: Deque[tuple[Tile, str]] = collections.deque([(grid.start, 'up')])
        seen = set()
        to_add = []
        while len(queue) != 0:
            t, from_dir = queue.popleft()
            seen.add(t)

            for attr in ['up', 'down', 'left', 'right']:
                if t.happy: continue
                if attr == from_dir: continue
                other = getattr(t, attr)

                if other in seen:
                    setattr(t, attr, None)
                    setattr(other, directional_inverses[attr], None)
                    continue
                if other:
                    to_add.append((other, directional_inverses[attr]))
            random.shuffle(to_add)
            for p in to_add: queue.append(p)
            to_add = []

            t.happy = True

            if step:
                print([f'({str(t)}, {dir})' for t, dir in queue])
                print(str(grid))
                input()
        for tile in (t for row in grid.grid for t in row): tile.happy = False
