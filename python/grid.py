class Tile:
    def __init__(self, i, j) -> None:
        self.happy = False
        self.up = None
        self.down = None
        self.left = None
        self.right = None
        self.i = i
        self.j = j

    def __str__(self) -> str:
        return f'{"." if self.happy else " "}, ({self.i} {self.j})'


class Grid:
    def __init__(self, n, m) -> None:
        self.n = n
        self.m = m
        self.grid = []
        self.start = Tile(0, 0)
        self.start.happy = True
        self.end = Tile(0, 0)
        self.end.happy = True

        for i in range(self.n):
            row = []
            for j in range(self.m):
                row.append(Tile(i, j))
                if i != 0:
                    self.grid[i-1][j].down = row[j]
                    row[j].up = self.grid[i-1][j]
                if j != 0:
                    row[j-1].right = row[j]
                    row[j].left = row[j-1]
            self.grid.append(row)
        self.start.down = self.grid[0][0]
        self.grid[0][0].up = self.start
        self.end.up = self.grid[n-1][m-1]
        self.grid[n-1][m-1].down = self.end
        self.grid[n-1][0].happy = True
        self.grid[0][m-1].happy = True

    def __str__(self) -> str:
        total = ''
        for i in range(self.n):
            # Top Layer
            if i == 0:
                # Top
                for j in range(self.m):
                    t = self.grid[i][j]
                    if j == 0:
                        match not t.up, not t.left:
                            case False, False: total += ' '
                            case False,  True: total += '╷'
                            case  True, False: total += '╶'
                            case  True,  True: total += '┌'
                    # +1+3+
                    # | 2 |
                    # +-+-+
                    match not t.up, not t.right, j < self.m-1 and not self.grid[i][j+1].up:
                        case False, False, False: total += '  '
                        case False, False,  True: total += ' ╶'
                        case False,  True, False: total += ' ╷'
                        case False,  True,  True: total += ' ┌'
                        case  True, False, False: total += '─╴'
                        case  True, False,  True: total += '──'
                        case  True,  True, False: total += '─┐'
                        case  True,  True,  True: total += '─┬'
                total += '\n'

                # TODO: Unindent

            # Middle
            for j in range(self.m):
                t = self.grid[i][j]
                if j == 0: total += '│' if not t.left else ' '
                total += '.' if t.happy else ' '
                total += '│' if not t.right else ' '
            total += '\n'
            # Bottom
            for j in range(self.m):
                t = self.grid[i][j]
                if j == 0:
                    match not t.down, not t.left, i < self.n-1 and not self.grid[i+1][j].left:
                        case False, False, False: total += ' '
                        case False, False,  True: total += '╷'
                        case False,  True, False: total += '╵'
                        case False,  True,  True: total += '│'
                        case  True, False, False: total += '╶'
                        case  True, False,  True: total += '┌'
                        case  True,  True, False: total += '└'
                        case  True,  True,  True: total += '├'
                # This is ugly.
                # +-+-+
                # | 2 |
                # +1+4+
                # | 3
                # +-+
                match not t.down, not t.right, i < self.n-1 and not self.grid[i+1][j].right, j < self.m-1 and not self.grid[i][j+1].down:
                    case False, False, False, False: total += '  '
                    case False, False, False,  True: total += ' ╶'
                    case False, False,  True, False: total += ' ╷'
                    case False, False,  True,  True: total += ' ┌'
                    case False,  True, False, False: total += ' ╵'
                    case False,  True, False,  True: total += ' └'
                    case False,  True,  True, False: total += ' │'
                    case False,  True,  True,  True: total += ' ├'
                    case  True, False, False, False: total += '─╴'
                    case  True, False, False,  True: total += '──'
                    case  True, False,  True, False: total += '─┐'
                    case  True, False,  True,  True: total += '─┬'
                    case  True,  True, False, False: total += '─┘'
                    case  True,  True, False,  True: total += '─┴'
                    case  True,  True,  True, False: total += '─┤'
                    case  True,  True,  True,  True: total += '─┼'
                    case _: total += '..'
            total += '\n'
        return total
