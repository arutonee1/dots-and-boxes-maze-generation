from grid import *
from alg import TwoStage


# WIDTH = 4
# HEIGHT = 4
WIDTH = 10
HEIGHT = 10
# WIDTH = 20
# HEIGHT = 20


if __name__ == '__main__':
    grid = Grid(HEIGHT, WIDTH)
    # TwoStage.run(grid)
    TwoStage.run(grid, True)
    print(str(grid))
