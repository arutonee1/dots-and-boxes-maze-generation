# Dots and Boxes Maze Generation
This is a maze generation algorithm I came up with one night after playing a few rounds of Dots and
Boxes against myself. As far as I know, this algorithm has not been implemented before.

There are two variants of this algorithm. One has 2 stages and one has 3. The 3-stage variant
should theoretically be faster. I will implement the 2-stage algorithm for simplicity.

A description of the algorithm will be added later.
