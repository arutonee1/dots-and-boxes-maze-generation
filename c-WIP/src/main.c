#include <stddef.h>
#include "alg.h"

#define WIDTH 5
#define HEIGHT 5

int main() {
	Tile ***grid = create_grid(HEIGHT, WIDTH);
	init(grid, HEIGHT, WIDTH);

	draw_line(grid, HEIGHT, WIDTH);
	print_grid(grid, HEIGHT, WIDTH);
	draw_line(grid, HEIGHT, WIDTH);
	print_grid(grid, HEIGHT, WIDTH);
	draw_line(grid, HEIGHT, WIDTH);
	print_grid(grid, HEIGHT, WIDTH);
	draw_line(grid, HEIGHT, WIDTH);
	print_grid(grid, HEIGHT, WIDTH);
	draw_line(grid, HEIGHT, WIDTH);
	print_grid(grid, HEIGHT, WIDTH);

	free_grid(grid, HEIGHT, WIDTH);
}
