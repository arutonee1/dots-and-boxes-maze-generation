#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "grid.h"

#define CORNER_CHAR '+'
#define HWALL_CHAR '-'
#define VWALL_CHAR '|'
#define BORDER_CHAR ' '
#define EMPTY_CHAR ' '

Tile *create_tile() {
	Tile *t = malloc(sizeof(Tile));
	t->happy = false;
	t->up = NULL;
	t->down = NULL;
	t->left = NULL;
	t->right = NULL;
	return t;
}

Tile ***create_grid(int n, int m) {
	Tile ***grid = malloc(n * sizeof(Tile **));
	for (int i = 0; i < n; ++i) {
		grid[i] = malloc(m * sizeof(Tile *));
		for (int j = 0; j < m; ++j) {
			grid[i][j] = create_tile();
			if (j != 0) {
				grid[i][j]->left = grid[i][j-1];
				grid[i][j-1]->right = grid[i][j];
			}
			if (i != 0) {
				grid[i][j]->up = grid[i-1][j];
				grid[i-1][j]->down = grid[i][j];
			}
		}
	}
	// NOTE: This is intentionally dangling
	grid[0][0]->up = create_tile();
	free(grid[0][0]->up);
	// NOTE: This is intentionally dangling
	grid[n-1][m-1]->down = create_tile();
	free(grid[n-1][m-1]->down);
	return grid;
}

void free_grid(Tile ***grid, int n, int m) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			free(grid[i][j]);
		free(grid[i]);
	}
	free(grid);
}

void print_grid(Tile ***grid, int n, int m) {
	for (int i = 0; i < n; ++i) {
		if (i == 0) {
			for (int j = 0; j < m; ++j) {
				Tile *t = grid[i][j];
				if (j == 0) putchar(!t->up || !t->left  ? CORNER_CHAR : BORDER_CHAR);
				putchar(!t->up              ? HWALL_CHAR : BORDER_CHAR);
				putchar(!t->up || !t->right || !t->right->up || !t->up->right ? CORNER_CHAR : BORDER_CHAR);
			}
			putchar('\n');
		}
		for (int j = 0; j < m; ++j) {
			Tile *t = grid[i][j];
			if (j == 0) putchar(!t->left ? VWALL_CHAR : BORDER_CHAR);
			// TODO: Remove debug happiness
			putchar(t->happy ? '.' : EMPTY_CHAR);
			putchar(!t->right ? VWALL_CHAR : BORDER_CHAR);
		}
		putchar('\n');
		for (int j = 0; j < m; ++j) {
			Tile *t = grid[i][j];
			if (j == 0) putchar(!t->down || !t->left  ? CORNER_CHAR : BORDER_CHAR);
			putchar(!t->down              ? HWALL_CHAR : BORDER_CHAR);
			putchar(!t->down || !t->right || !t->right->down || !t->down->right ? CORNER_CHAR : BORDER_CHAR);
		}
		putchar('\n');
	}
}
