#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <stdlib.h>
#include "alg.h"

static int remaining_unhappy;

void init(Tile ***grid, int n, int m) {
	// TODO: Remove debug srand

	srand(0);
	//srand(time(NULL));

	grid[0][m-1]->happy = true;
	grid[n-1][0]->happy = true;
	remaining_unhappy = n * m - 2;
}

Tile *random_unhappy(Tile ***grid, int n, int m) {
	int rng = rand() % remaining_unhappy;
	printf("rng: %d\n", rng);
	--remaining_unhappy;
	int i, j, counter = 0;
	for (; counter < rng;) {
		for (i = 0; i < n; ++i) {
			for (j = 0; j < m; ++j) {
				if (!grid[i][j]->happy) ++counter;
				if (counter >= rng) break;
			}
			if (counter >= rng) break;
		}
	}
	return grid[i][j];
}
