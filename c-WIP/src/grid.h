#include <stdbool.h>

struct objTile {
    bool happy;
    struct objTile *up;
    struct objTile *down;
    struct objTile *left;
    struct objTile *right;
};
typedef struct objTile Tile;
Tile ***create_grid(int n, int m);
void free_grid(Tile ***grid, int n, int m);
void print_grid(Tile ***grid, int n, int m);
